require('dotenv').config()
const port = 8080

import Koa from 'koa'
import KoaRouter from 'koa-router'
import bodyParser from 'koa-bodyparser'
import '@babel/polyfill'
import {home, list, add, update, reset, connect} from "./api";

const app = new Koa()
const router = new KoaRouter()

app.use(bodyParser())
router.get('/', home)
router.get('/list', list)
router.post('/add', add)
router.put('/update', update)
router.delete('/reset', reset)
router.get('/connect', connect)

app.use(router.routes()).use(router.allowedMethods())
app.listen(port, () => {
    console.log('App is running')
})
