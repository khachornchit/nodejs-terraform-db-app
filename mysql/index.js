require('dotenv').config()
const mysql = require('mysql');

const pool = mysql.createPool({
    host: process.env.host,
    user: process.env.username,
    password: process.env.password,
    database: process.env.database,
    port: process.env.port,
    ssl: true
});

class Mysql {
    constructor() {

    }

    query() {
        return new Promise((resolve, reject) => {
            pool.query('SELECT NOW() AS cTime', function (error, results) {
                if (error) {
                    throw error
                }

                resolve(`Current time is: ${results[0].cTime}`)
            }, function () {
                reject(`Connection error !`)
            });
        })

    }
}

module.exports = new Mysql()
