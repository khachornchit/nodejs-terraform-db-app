import {init, data} from "../data";
const mysql = require('../mysql')

export async function home(ctx) {
    ctx.body = `Hello from ${process.env.host}`
}

export async function list(ctx) {
    ctx.body = data
}

export async function add(ctx) {
    let input = ctx.request.body
    data.push(input)
    ctx.body = data
}

export async function update(ctx) {
    let input = ctx.request.body
    const index = data.findIndex(e => e.id === input.id)

    if (index === -1) {
        data.push(input)
    } else {
        data[index] = input
    }

    ctx.body = data
}

export async function reset(ctx) {
    data = init
    ctx.body = data
}

export async function connect(ctx) {
    let data = await mysql.query()
    ctx.body = {
        "message": `Starting MySQL connection....`,
        "data": data,
    }
}
