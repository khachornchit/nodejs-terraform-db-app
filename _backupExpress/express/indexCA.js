require('dotenv').config()

const express = require('_backupExpress/express/index')
const os = require('os')
const app = express()
const mysql = require('mysql')
const fs = require("fs");

const getConnection = async () => {

    const client = mysql.createConnection({
        host: process.env.host,
        user: process.env.username,
        password: process.env.password,
        database: process.env.database,
        port: process.env.port,
        ssl: {
            ca: fs.readFileSync('mysql/ca-certificate.cer')
        }
    })

    client.connect()
    return client
}

app.get('/', (req, res) => {
    res.send(`Hello world CA from ${os.hostname()}`)
})

app.get('/time', async (req, res) => {

    const connection = await getConnection()

    await connection.query('SELECT NOW() AS cTime', (error, results) => {
        if (error) throw error
        res.send(`Current time is: ${results[0].cTime}`)
    })

    await connection.end()
})

app.listen(8080)
