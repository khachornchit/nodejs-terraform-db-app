require('dotenv').config()

const express = require('_backupExpress/express/index')
const os = require('os')
const app = express()
const mysql = require('mysql')

const getConnection = async () => {

    const client = mysql.createConnection({
        host: process.env.host,
        user: process.env.username,
        password: process.env.password,
        database: process.env.database,
        port: process.env.port,
        ssl: true
    })

    client.connect()
    return client
}

app.get('/', async (req, res) => {

    res.send(`Hello world from ${os.hostname()}.`)
})

app.get('/time', async (req, res) => {
    const client = await getConnection()

    client.query('SELECT NOW() AS cTime', (error, results) => {
        if (error) throw error
        res.send(`Current time is: ${results[0].cTime}`)
    })

    client.end()
})

app.listen(8080)
