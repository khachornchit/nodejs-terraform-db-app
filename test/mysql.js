require('dotenv').config()
const mysqlCA = require('mysql')

const getConnection = async () => {

    const client = mysqlCA.createConnection({
        host: process.env.host,
        user: process.env.username,
        password: process.env.password,
        database: process.env.database,
        port: process.env.port,
        ssl: true
    })

    client.connect()
    return client
}

const getTime = async () => {
    const connection = await getConnection()

    await connection.query('SELECT NOW() AS cTime', (error, results) => {
        if (error) throw error
        console.log(`Current time is: ${results[0].cTime}`)
    })

    connection.end()
}

getTime().then(x => {
})
