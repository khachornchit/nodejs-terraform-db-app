require('dotenv').config()
const mysqlCA = require('mysql')
const fs = require("fs");

const getConnection = async () => {

    const client = mysqlCA.createConnection({
        host: process.env.host,
        user: process.env.username,
        password: process.env.password,
        database: process.env.database,
        port: process.env.port,
        ssl: {
            ca: fs.readFileSync('./mysql/ca-certificate.cer')
        }
    })

    client.connect()
    return client
}

const getTime = async () => {
    const connection = await getConnection()

    await connection.query('SELECT NOW() AS cTime', (error, results) => {
        if (error) throw error
        console.log(`Current time is: ${results[0].cTime}`)
    })

    connection.end()
}

getTime().then(x => {
})
